import moment from "moment";

require('./bootstrap');
require('moment/moment');
require('bootstrap-select/js/bootstrap-select');
require('bootstrap-datepicker/js/bootstrap-datepicker');


$('select').selectpicker();

$('.datepicker').each(function (i, e) {
    $(e).datepicker({
        format: 'dd.mm.yyyy',
    });
});


$('input.timepicker').each(function (i, e) {
    $(e).val(moment($(e).val(), 'HH:mm:ss').locale('cs').format('LTS'));
});

$('input.timepicker').on('change', function (e) {
    var val = moment($(this).val(), 'HH:mm:ss').locale('cs').format('LTS');
    if(val == 'Invalid date'){
        $(this).val( moment($(this).data('time'),'HH:mm:ss').locale('cs').format('LTS'));
        return;
    }
    $(this).val(val);
    $(this).data('time',val);
});

$(document).ready(function(){
    if( $('#datepicker_from_active').length !=0) {
        update();
        setInterval(update, 1000);
    }
});


var update = function () {
    var now = moment().locale('cs');
    var then = $('#datepicker_from_active').val() +' '+ $('#from').val();
    var ms =now.diff(moment(then,"DD.MM.YYYY HH:mm:ss"));
    var d = moment.duration(ms);
    var s = Math.floor(d.asHours()) + moment.utc(ms).format(":mm:ss");
    $('.attendance__form__current').html(s);
    $(document).prop('title', s);
};