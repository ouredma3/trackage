<div class="header">
    <div class="header__logo">
        <a href="/"><img  class="header__logo__small" src="{{asset('images/logo-type.svg')}}" alt="logo"> <img class="header__logo__full" src="{{asset('images/logo.svg')}}" alt="Track Age"></a>
    </div>
    <div class="header__menu">
        <ul>
            @auth


                    @if(Auth::user()->role == 'admin')
                        <li>
                            <a href="{{ url('/projects') }}" class="project-manager">Project Manager</a>
                        </li>
                    @endif
                    <li>
                        <a href="{{ route('attendance') }}" class="attendance">Attendance</a>
                    </li>
                @if(Auth::user()->role == 'admin')
                    <li>
                        <a href="{{ url('/users') }}"  class="user-manager">User Manager</a>
                    </li>
                @endif
                        <li>
                            <a href="{{ url('/profile') }}" class="profile">Profile</a>
                        </li>

                    @endauth
                        <li>
                <a href="{{ route('logout') }}" class="logout">Logout</a>
                        </li>
        </ul>
    </div>
</div>