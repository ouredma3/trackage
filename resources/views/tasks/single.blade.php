@extends('layouts.app')

@section('content')
    <div class="container">

        <h1>{{ __('Edit task') }}</h1>
        <div class="container__wrapper">
            <div class="container__wrapper__module usercard">
                <form method="POST" action="{{ url('/tasks/'.$task->id) }}">
                    @csrf
                    <label for="name"> {{ __('Name') }}</label>
                    <div class="input-group">
                        <input id="name" type="text" name="name" required
                               class="form-control @error('name') is-invalid @enderror" value="{!! $task->name !!}"/>
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <label for="project"> {{ __('Project') }}</label>
                    <div class="input-group">
                        <input id="project" type="text"
                               class="form-control @error('project') is-invalid @enderror" value="{!! $project->name !!}" disabled/>
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <label for="estimate"> {{ __('Estimate') }}</label>
                    <div class="input-group">
                        <input id="estimate" type="number" name="estimate" required
                               class="form-control @error('estimate') is-invalid @enderror" value="{!! $task->estimate !!}"/>
                        @error('estimate')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <label for="actual"> {{ __('Real') }}</label>
                    <div class="input-group">
                        <input id="actual" type="number" name="actual" required
                               class="form-control @error('actual') is-invalid @enderror" value="{!! $task->actual !!}"/>
                        @error('actual')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="button-box">
                        <input class="btn" type="submit" value="{{ __('Save') }}"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
