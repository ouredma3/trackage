@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Dashboard</h1>
    <div class="container__wrapper">
        <div class="attendance__form usercard container__wrapper__module">
            @if(isset($activeAttendance) && $activeAttendance != null)
                <form method="POST" action="{{ route('endAttendance') }}">
                    <h2>{{ __('End attendance') }}</h2>
                    @csrf
                    <table class="attendance__form__table attendance__form__table--no_project">
                        <thead>
                        <tr>
                            <th>
                                <label for="description"> {{ __('Description') }}</label>
                            </th>
                            <th>
                                <label for="task_id"> {{ __('Task') }}</label>
                            </th>
                            <th>
                                <label for="from"> {{ __('From') }}</label>
                            </th>
                            <th>
                                <span class="attendance__form__current_label">
                                    {{ __('Current') }}
                                </span>
                            </th>
                            <th>

                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="attendance__description">
                                <input id="description" type="text" name="description"
                                       class="form-control" value="{!! $activeAttendance['description'] !!}"/>
                            </td>
                            <td class="attendance__project">
                                <select id="task_id" name="task_id">
                                    @foreach($tasks as$task)
                                        <option value="{!! $task->id !!}" @if($activeAttendance['task_id'] == $task->id) selected @endif>{!! $task->name !!}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td class="attendance__from">
                                <div class="attendance__list__day__item__stats__time__from">
                                    <input id="from" type="text" name="from"
                                           class="form-control" value="{!! $activeAttendance['from'] !!}"/>

                                    <div class="datepicker__box">
                                        <input type='text' class="form-control datepicker" id="datepicker_from_active"
                                               name="from_date"
                                               value="{!! date("d.m.Y", strtotime($activeAttendance['from_date'])) !!}"/>
                                        <label class="datepicker__icon" for="datepicker_from_active"></label>
                                    </div>
                                </div>
                            </td>
                            <td class="attendance__current">
                                <div class="attendance__form__current"></div>
                            </td>
                            <td class="attendance__submit">
                                <button class="btn" type="submit"> {{ __('Stop') }}</button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            @else
                <form method="POST" action="{{ route('startAttendance') }}">
                    <h2>{{ __('Start attendance') }}</h2>
                    @csrf
                    <table class="attendance__form__table attendance__form__table--no_project">
                        <thead>
                        <tr>
                            <th>
                                <label for="description"> {{ __('Description') }}</label>
                            </th>
                            <th>
                                <label for="task_id"> {{ __('Task') }}</label>
                            </th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="attendance__description">
                                <input id="description" type="text" name="description"
                                       class="form-control" value=""/>
                            </td>
                            <td class="attendance__project">
                                <select id="task_id" name="task_id">
                                    @foreach($tasks as$task)
                                        <option value="{!! $task->id !!}">{!! $task->name !!}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td class="attendance__from"></td>
                            <td class="attendance__current"></td>
                            <td class="attendance__submit">
                                <button class="btn" type="submit"> {{ __('Start') }}</button>
                            </td>
                        </tr>
                        </tbody>
                    </table>


                </form>
            @endif
        </div>

                <div class="usercard usercard--attendance container__wrapper__module">
                    <h2>{{ __('Users attendance') }}</h2>
                    @if(isset($attendance))
                        @foreach($attendance as $day)
                            <div class="attendance__list__day">
                                @foreach($day['items'] as $item)
                                    @if ($loop->first)
                                        <div class="attendance__list__day__header">
                                            <div class="attendance__list__day__header__day">
                                                {!! $day['day'] !!}, {!! $day['date'] !!}
                                            </div>
                                            <div class="attendance__list__day__header__total">
                                                Total:
                                                <span class="attendance__list__day__header__total__number">
                                    @php
                                        echo sprintf('%02d', floor($day['total'] / 3600)) . ':'. sprintf('%02d', floor(($day['total'] / 60) %60 )) .':'. sprintf('%02d', $day['total'] % 60 );
                                    @endphp
                                    </span>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="attendance__list__day__item">
                                        <div  class="attendance__list__day__item__form">

                                            <div class="attendance__list__day__item__info">
                                                @if(isset($tasks[$item['task_id']]) && !empty($tasks[$item['task_id']]))
                                                <div class="attendance__list__day__item__stats__project">
                                                    {!! $tasks[$item['task_id']]->name!!} |
                                                </div>
                                                @endif
                                                <div  class="attendance__list__day__item__info__description"> &nbsp;{!!  $item['description'] !!}</div>

                                            </div>
                                            <div class="attendance__list__day__item__stats">

                                                <div class="attendance__list__day__item__stats__time__form">
                                                    <div class="attendance__list__day__item__stats__time__from">
                                                        <div> {!! $item['from'] !!} - </div>
                                                        <div> {!! $item['to'] !!}</div>
                                                    </div>
                                                </div>
                                                <div class="attendance__list__day__item__stats__time__total">
                                                    @php
                                                        echo sprintf('%02d', floor($item['time'] / 3600)) . ':'. sprintf('%02d', floor(($item['time'] / 60) %60 )) .':'. sprintf('%02d', $item['time'] % 60 );
                                                    @endphp
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endforeach
                    @else
                        <div class="attendance__list__empty">{{ __('No records found') }}</div>
                    @endif
                </div>
            </div>
    </div>
</div>
@endsection
