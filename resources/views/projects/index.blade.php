@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>{{ __('Project  manager') }}</h1>
        <div class="container__wrapper">
        <div class="container__wrapper__module usercard">
            <h2>All projects</h2>
            <table class="user__listing">
                <tr>
                    <th>{{ __('Name') }}</th>
                    <th>{{ __('Actions') }}</th>
                </tr>
                @foreach($projects as $project)
                    <tr class="user__listing__row">
                        <td class="user__name">
                            {!! $project->name !!}
                        </td>
                        <td class="user__actions">
                            <a class="user__action user__action--edit" href="{{ url('/projects/'.$project->id.'') }}">View</a>
                            <a class="user__action user__action--delete" href="{{ url('/projects/'.$project->id.'/delete') }}">Delete</a>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
        <div class="container__wrapper__module usercard">
            <h2>Add new project</h2>
            <form method="POST" action="{{  route('addProjects') }}">
                @csrf
                <label for="name"> {{ __('Name') }}</label>
                <div class="input-group">
                    <input id="name" type="text" name="name" required autocomplete="name"
                           class="form-control @error('name') is-invalid @enderror"/>
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
                <div class="button-box">
                    <input class="btn" type="submit" value="{{ __('Save') }}"/>
                </div>
            </form>
        </div>
        </div>
    </div>
@endsection
