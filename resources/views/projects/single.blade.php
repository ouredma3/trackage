@extends('layouts.app')

@section('content')
    <div class="container">

        <h1>{{ __('Edit project') }}</h1>
        <div class="container__wrapper">
            <div class="container__wrapper__module usercard">
                <form method="POST" action="{{ url('/projects/'.$project->id) }}">
                    @csrf
                    <label for="name"> {{ __('Name') }}</label>
                    <div class="input-group">
                        <input id="name" type="text" name="name" required autocomplete="name"
                               class="form-control @error('name') is-invalid @enderror" value="{!! $project->name !!}"/>
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="button-box">
                        <input class="btn" type="submit" value="{{ __('Save') }}"/>
                    </div>
                </form>
            </div>
            <div class="container__wrapper__module usercard">
                <h2>Tasks</h2>
                <table class="user__listing">
                    <tr>
                        <td>{{ __('Name') }}</td>
                        <td>{{ __('Estimate') }}</td>
                        <td>{{ __('Real') }}</td>
                        <td>%</td>
                        <td>{{ __('Actions') }}</td>
                    </tr>
                    @foreach($tasks as $task)
                        <tr class="user__listing__row">
                            <td class="user__name">
                                {!! $task->name !!}
                            </td>
                            <td class="user__name">
                                {!! $task->estimate !!} h
                            </td>
                            <td class="user__name">
                                {!! $task->actual !!} h
                            </td>
                            <td>
                                <div class="percentage @if($task->percentage <= 100)percentage--{!! round($task->percentage) !!} @else percentage--over @endif">
                                <span>{!! $task->percentage  !!} %</span>
                                </div>
                            </td>
                            <td class="user__actions">
                                <a class="user__action user__action--edit"
                                   href="{{ url('/tasks/'.$task->id.'') }}">View</a>
                                <a class="user__action user__action--delete" href="{{ url('/tasks/'.$task->id.'/delete') }}">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <div class="container__wrapper__module usercard">
                <h2>Add new Task</h2>
                <form method="POST" action="{{  route('addTask') }}">
                    @csrf
                    <label for="name"> {{ __('Name') }}</label>
                    <div class="input-group">
                        <input id="name" type="text" name="name" required
                               class="form-control @error('name') is-invalid @enderror" value=""/>
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <label for="project-name"> {{ __('Project') }}</label>
                    <div class="input-group">
                        <input id="project-name" type="text"
                               class="form-control @error('project-name') is-invalid @enderror" value="{!! $project->name !!}" disabled/>
                        @error('project-name')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                        <input id="project_id" name="project_id" type="number" value="{!! $project->id !!}" hidden/>

                    <label for="estimate"> {{ __('Estimate') }}</label>
                    <div class="input-group">
                        <input id="estimate" type="number" name="estimate" required autocomplete="name"
                               class="form-control @error('estimate') is-invalid @enderror" value=""/>
                        @error('estimate')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <label for="actual"> {{ __('Real') }}</label>
                    <div class="input-group">
                        <input id="actual" type="number" name="actual"
                               class="form-control @error('actual') is-invalid @enderror" value=""/>
                        @error('actual')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="button-box">
                        <input class="btn" type="submit" value="{{ __('Create') }}"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
