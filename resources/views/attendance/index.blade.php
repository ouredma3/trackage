@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="attendance__form usercard ">
            @if(isset($activeAttendance) && $activeAttendance != null)
                <form method="POST" action="{{ route('endAttendance') }}">
                    @csrf
                    <table class="attendance__form__table attendance__form__table--no_project">
                        <thead>
                        <tr>
                            <th>
                                <label for="description"> {{ __('Description') }}</label>
                            </th>
                            <th>
                                <label for="task_id"> {{ __('Task') }}</label>
                            </th>
                            <th>
                                <label for="from"> {{ __('From') }}</label>
                            </th>
                            <th>
                                <span class="attendance__form__current_label">
                                    {{ __('Current') }}
                                </span>
                            </th>
                            <th>

                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="attendance__description">
                                <input id="description" type="text" name="description"
                                       class="form-control" value="{!! $activeAttendance['description'] !!}"/>
                            </td>
                            <td class="attendance__project">
                                <select id="task_id" name="task_id">
                                        @foreach($newtasks as $tasks)
                                            <optgroup label="{!! $projects[$tasks[0]->project_id]->name !!}">
                                                @foreach($tasks as $task)
                                                    <option value="{!! $task->id !!}"
                                                            @if($activeAttendance['task_id'] == $task->id) selected @endif>{!! $task->name !!}</option>
                                                @endforeach
                                            </optgroup>
                                        @endforeach
                                </select>
                            </td>
                            <td class="attendance__from">
                                <div class="attendance__list__day__item__stats__time__from">
                                    <input id="from" type="text" name="from"
                                           class="form-control timepicker" value="{!! $activeAttendance['from'] !!}"/>

                                    <div class="datepicker__box">
                                        <input type='text' class="form-control datepicker" id="datepicker_from_active"
                                               name="from_date"
                                               value="{!! date("d.m.Y", strtotime($activeAttendance['from_date'])) !!}"/>
                                        <label class="datepicker__icon" for="datepicker_from_active"></label>
                                    </div>
                                </div>
                            </td>
                            <td class="attendance__current">
                                <div class="attendance__form__current"></div>
                            </td>
                            <td class="attendance__submit">
                                <button class="btn" type="submit"> {{ __('Stop') }}</button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            @else
                <form method="POST" action="{{ route('startAttendance') }}">
                    @csrf
                    <table class="attendance__form__table attendance__form__table--no_project">
                        <thead>
                        <tr>
                            <th>
                                <label for="description"> {{ __('Description') }}</label>
                            </th>
                            <th>
                                <label for="task_id"> {{ __('Task') }}</label>
                            </th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="attendance__description">
                                <input id="description" type="text" name="description"
                                       class="form-control" value=""/>
                            </td>
                            <td class="attendance__project">
                                <select id="task_id" name="task_id">
                                    @foreach($newtasks as $tasks)
                                        <optgroup label="{!! $projects[$tasks[0]->project_id]->name !!}">
                                            @foreach($tasks as $task)
                                                <option value="{!! $task->id !!}">{!! $task->name !!}</option>
                                            @endforeach
                                        </optgroup>
                                    @endforeach
                                </select>
                            </td>
                            <td class="attendance__from"></td>
                            <td class="attendance__current"></td>
                            <td class="attendance__submit">
                                <button class="btn" type="submit"> {{ __('Start') }}</button>
                            </td>
                        </tr>
                        </tbody>
                    </table>


                </form>
            @endif
        </div>
        <div class="attendance__list usercard ">
            @if(isset($attendance))
                @foreach($attendance as $day)
                    <div class="attendance__list__day">
                        @foreach($day['items'] as $item)
                            @if ($loop->first)
                                <div class="attendance__list__day__header">
                                    <div class="attendance__list__day__header__day">
                                        {!! $day['day'] !!}, {!! $day['date'] !!}
                                    </div>
                                    <div class="attendance__list__day__header__total">
                                        Total:
                                        <span class="attendance__list__day__header__total__number">
                                    @php
                                        echo sprintf('%02d', floor($day['total'] / 3600)) . ':'. sprintf('%02d', floor(($day['total'] / 60) %60 )) .':'. sprintf('%02d', $day['total'] % 60 );
                                    @endphp
                                    </span>
                                    </div>
                                </div>
                            @endif
                            <div class="attendance__list__day__item">
                                <form class="attendance__list__day__item__form" method="POST"
                                      action="{{ url('editAttendance/'.$item['id'].'') }}">
                                    @csrf
                                    <div class="attendance__list__day__item__info">
                                        <input type="text" name="description"
                                               class="attendance__list__day__item__info__description form-control"
                                               value=" {!! $item['description'] !!}"/>
                                        <div class="attendance__list__day__item__stats__project">
                                            <select id="task_id" name="task_id">
                                                @foreach($newtasks as $tasks)
                                                    <optgroup label="{!! $projects[$tasks[0]->project_id]->name !!}">
                                                        @foreach($tasks as $task)
                                                            <option value="{!! $task->id !!}"
                                                                    @if($item['task_id'] == $task->id) selected @endif>{!! $task->name !!}</option>
                                                        @endforeach
                                                    </optgroup>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="attendance__list__day__item__stats">

                                        <div class="attendance__list__day__item__stats__time__form">
                                            <div class="attendance__list__day__item__stats__time__from">
                                                <input type='text' class="form-control timepicker"
                                                       data-time="{!! $item['from'] !!}"
                                                       name="from"
                                                       value="{!! $item['from'] !!}"/>
                                                <div class='datepicker__box'>
                                                    <input type='text' class="form-control datepicker"
                                                           name="from_date"
                                                           id="datepicker_from_{!! $loop->index !!}"
                                                           value="{!! date("d.m.Y", strtotime($item['from_date'])) !!}"/>
                                                    <label class="datepicker__icon"
                                                           for="datepicker_from_{!! $loop->index !!}"></label>
                                                </div>
                                            </div>
                                            <div class="attendance__list__day__item__stats__time__to">
                                                <input type='text' class="form-control timepicker"
                                                       data-time="{!! $item['to'] !!}"
                                                       name="to"
                                                       value=" {!! $item['to'] !!}"/>
                                                <div class='datepicker__box'>
                                                    <input type='text' class="form-control datepicker"
                                                           id="datepicker_to_{!! $loop->index !!}"
                                                           name="to_date"
                                                           value="{!! date("d.m.Y", strtotime($item['to_date'])) !!}"/>
                                                    <label class="datepicker__icon"
                                                           for="datepicker_to_{!! $loop->index !!}"></label>
                                                </div>
                                            </div>
                                            <button class="attendance__list__day__item__button" type="submit">Save
                                            </button>
                                        </div>
                                        <div class="attendance__list__day__item__stats__time__total">
                                            @php
                                                echo sprintf('%02d', floor($item['time'] / 3600)) . ':'. sprintf('%02d', floor(($item['time'] / 60) %60 )) .':'. sprintf('%02d', $item['time'] % 60 );
                                            @endphp
                                        </div>
                                    </div>
                                </form>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            @else
                <div class="attendance__list__empty">{{ __('No records found') }}</div>
            @endif
        </div>
    </div>
@endsection
