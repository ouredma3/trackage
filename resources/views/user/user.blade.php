@extends('layouts.app')

@section('content')
    <div class="container">

        <h1>{{ __('User detail') }}</h1>
        <div class="container__wrapper">
        <div class="usercard container__wrapper__module">
            <h2>{{ __('User information') }}</h2>
            <form method="POST" action="{{ url('/users/'.$user->id) }}">
                @csrf
                <label for="name"> {{ __('Name') }}</label>
                <div class="input-group">
                    <input id="name" type="text" name="name" required autocomplete="name"
                           class="form-control @error('name') is-invalid @enderror" value="{!! $user->name !!}"/>
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
                <label for="email"> {{ __('Email') }}</label>
                <div class="input-group">
                    <input id="email" type="email" name="email" required autocomplete="email"
                           class="form-control @error('email') is-invalid @enderror" value="{!! $user->email !!}"/>
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
                <label for="role">{{ __('Role') }}</label>
                <div class="input-group">
                <select id="role" name="role">
                    <option value="admin" @if($user->role == "admin") selected @endif>admin</option>
                    <option value="user" @if($user->role == "user") selected @endif>user</option>
                </select>
                </div>
                <div class="button-box">
                <input class="btn btn-primary" type="submit" value="{{ __('Save') }}"/>
                </div>
            </form>
        </div>
        <div class="usercard usercard--attendance container__wrapper__module">
            <h2>{{ __('Users attendance') }}</h2>
            @if(isset($attendance))
                @foreach($attendance as $day)
                    <div class="attendance__list__day">
                        @foreach($day['items'] as $item)
                            @if ($loop->first)
                                <div class="attendance__list__day__header">
                                    <div class="attendance__list__day__header__day">
                                        {!! $day['day'] !!}, {!! $day['date'] !!}
                                    </div>
                                    <div class="attendance__list__day__header__total">
                                        Total:
                                        <span class="attendance__list__day__header__total__number">
                                    @php
                                        echo sprintf('%02d', floor($day['total'] / 3600)) . ':'. sprintf('%02d', floor(($day['total'] / 60) %60 )) .':'. sprintf('%02d', $day['total'] % 60 );
                                    @endphp
                                    </span>
                                    </div>
                                </div>
                            @endif
                            <div class="attendance__list__day__item">
                                <div  class="attendance__list__day__item__form">

                                    <div class="attendance__list__day__item__info">
                                        <div  class="attendance__list__day__item__info__description"> {!! $item['description'] !!}</div>
                                        <div class="attendance__list__day__item__stats__project"></div>
                                    </div>
                                    <div class="attendance__list__day__item__stats">

                                        <div class="attendance__list__day__item__stats__time__form">
                                            <div class="attendance__list__day__item__stats__time__from">
                                                <div>{!! $item['from'] !!}</div>
                                            </div>
                                            <div class="attendance__list__day__item__stats__time__to">
                                                <div>{!! $item['to'] !!}</div>
                                            </div>
                                        </div>
                                        <div class="attendance__list__day__item__stats__time__total">
                                            @php
                                                echo sprintf('%02d', floor($item['time'] / 3600)) . ':'. sprintf('%02d', floor(($item['time'] / 60) %60 )) .':'. sprintf('%02d', $item['time'] % 60 );
                                            @endphp
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            @else
                <div class="attendance__list__empty">{{ __('No records found') }}</div>
            @endif
        </div>
        </div>
    </div>
@endsection
