@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>{{ __('User  manager') }}</h1>
        <div class="usercard">
            <table class="user__listing">
                <tr>
                    <th>{{ __('Name') }}</th>
                    <th>{{ __('Email') }}</th>
                    <th>{{ __('Actions') }}</th>
                </tr>
                @foreach($users as $user)
                    <tr class="user__listing__row">
                        <td class="user__name">
                            {!! $user["name"] !!}
                        </td>
                        <td class="user__email">
                            {!! $user["email"] !!}
                        </td>
                        <td class="user__actions">
                            <a class="user__action user__action--edit" href="{{ url('/users/'.$user["id"].'') }}">View</a>
                            <a class="user__action user__action--delete" href="{{ url('/users/'.$user["id"].'/delete') }}">Delete</a>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection
