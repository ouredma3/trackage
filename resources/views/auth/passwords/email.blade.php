<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/main.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Baloo+2:400,600,700|Roboto:400,700&display=swap&subset=latin-ext"
          rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div class="index">
    <div class="index__box">
        <div class="index__box__logo"><img class="index__box__logo__small" src="{{asset('images/logo-type.svg')}}"
                                           alt="logo"> <img class="index__box__logo__full"
                                                            src="{{asset('images/logo.svg')}}" alt="Track Age"></div>
        <div class="index__box__form">
                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group">
                            <label for="email">{{ __('E-Mail Address') }}</label>


                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>


                        <div class="form-group">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            @if (Route::has('login'))
                                <a class="form-link" href="{{ route('login') }}">
                                    {{ __('Back to login') }}
                                </a>
                            @endif
                        </div>
                    </form>
        </div>
    </div>
</div>
</body>
