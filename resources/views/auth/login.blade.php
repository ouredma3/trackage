<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Baloo+2:400,600,700|Roboto:400,700&display=swap&subset=latin-ext" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div class="index">
    @include('flash::message')
    <div class="index__box">
        <div class="index__box__logo"><img class="index__box__logo__small" src="{{asset('images/logo-type.svg')}}"
                                           alt="logo"> <img class="index__box__logo__full"
                                                            src="{{asset('images/logo.svg')}}" alt="Track Age"></div>
        <div class="index__box__form">
            <a href="{{ route('authenticate') }}" class="btn btn">Login</a>
        </div>
    </div>
</div>
</body>