<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login/auth', 'Auth\LoginController@redirectToProvider')->name('authenticate');
Route::get('/callback', 'Auth\LoginController@handleProviderCallback');

Auth::routes();
Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/changePassword', function () {
    return View('auth.passwords.change'); // Your Blade template name
});

Route::post('/changePassword', 'UserController@changePassword')->name('changePassword');

Route::get('profile', 'UserController@getProfile');

Route::get('logout', 'UserController@logout');

Route::post('profile', 'UserController@updateProfile')->name('profile');

Route::get('attendance', 'AttendanceController@getMyAttendence')->name('attendance');

Route::post('startAttendance', 'AttendanceController@startAttendance')->name('startAttendance');

Route::post('editAttendance/{id}', 'AttendanceController@editAttendance');

Route::post('endAttendance', 'AttendanceController@endAttendance')->name('endAttendance');

Route::get('/tasks', 'ProjectController@getTasks');


Route::middleware('admin')->group(function () {

    Route::get('/users/{id}', 'UserController@getUser');

    Route::post('/users/{id}', 'UserController@updateUser');

    Route::delete('/users/{id}', 'UserController@deleteUser');

    Route::get('/users', 'UserController@allUsers');

    Route::get('/attendance/{id}', 'AttendanceController@getAttendance');

    Route::put('/users/{id}', 'UserController@updateUser');

    Route::put('/users/{id}/delete', 'UserController@deleteUser');

    Route::get('/projects', 'ProjectController@allProjects');

    Route::post('/projects', 'ProjectController@addProjects')->name('addProjects');

    Route::post('/tasks', 'ProjectController@addTask')->name('addTask');

    Route::post('/projects/{id}', 'ProjectController@editProject');

    Route::get('/projects/{id}', 'ProjectController@getProject');

    Route::get('/projects/{id}/delete', 'ProjectController@deleteProject');

    Route::get('/tasks/{id}', 'ProjectController@getTask');

    Route::get('/tasks/{id}/delete', 'ProjectController@deleteTask');

    Route::post('/tasks/{id}', 'ProjectController@editTask');
});


