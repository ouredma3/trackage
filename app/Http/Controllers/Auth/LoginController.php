<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;




    /**
     * Redirect the user to the Passport authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('laravelpassport')->redirect();
    }

    /**
     * Obtain the user information from Passport
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback(Request $request)
    {

        try {
            $userSocial  = Socialite::driver('laravelpassport')->stateless()->user();
        } catch (\Exception $e) {
            flash('User login failed');
            return redirect('/login')->withErrors($e->getMessage());
        }


        $user  =   User::where(['user_id' => $userSocial->getId()])->first();

        if($user){
            $user->role  = $userSocial->user['role'];
            $user->name  = $userSocial->getName();
            $user->email  = $userSocial->getEmail();
            $user->access_token  = $userSocial->accessTokenResponseBody["access_token"];
            $user->refresh_token  = $userSocial->accessTokenResponseBody["refresh_token"];
            Auth::login($user,true);

        }else{
            $user = User::create([
                'email'         => $userSocial->getEmail(),
                'name'         => $userSocial->getName(),
                'user_id'   => $userSocial->getId(),
                'access_token'   => $userSocial->accessTokenResponseBody["access_token"],
                'refresh_token'   => $userSocial->accessTokenResponseBody["refresh_token"],
                'role'  => $userSocial->user['role']
            ]);
            Auth::login($user,true);

        }
        //session(['current_user' => $userSocial->user]);
        return redirect()->route('home');

    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('guest')->except('logout');
    }
}
