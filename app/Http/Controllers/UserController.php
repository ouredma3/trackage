<?php

namespace App\Http\Controllers;

use App\Services\Users;
use Illuminate\Support\Facades\Auth;
use App\Services\Attendance;
use  App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    public $attendanceService;
    public $usersService;

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct(Attendance $attendanceService, Users $usersService)
    {
        $this->middleware('auth');
        $this->attendanceService = $attendanceService;
        $this->usersService = $usersService;

    }

    /**
     * Get the authenticated User.
     *
     * @return Response
     */
    public function getProfile()
    {
        try {
            return view('user.profile')->with('user', json_decode($this->usersService->getProfile()));
        } catch (\Exception $e) {
            flash("User service problem");
            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    /**
     * Get all Users.
     *
     * @return Response
     */
    public function allUsers()
    {
        try {
            return view('user.listing')->with('users', json_decode($this->usersService->allUsers(),true));
        } catch (\Exception $e) {
            flash("User service problem");
            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    /**
     * Get user by id.
     *
     * @return Response
     */
    public function getUser($id)
    {
        try {
            $attendance = $this->attendanceService->getAttendance($id);
        } catch (\Exception $e) {
            flash("Attendance service problem");
            return back()->with('error', $e->getMessage())->withInput();
        }
        try {
            $user = json_decode($this->usersService->getUser($id));
        } catch (\Exception $e) {
            flash("User service problem");
            return back()->with('error', $e->getMessage())->withInput();
        }
        return view('user.user')->with(['user' => $user, 'attendance' => $attendance]);

    }

    /**
     * Update authenticated user data
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateProfile(Request $request)
    {

        try {
            $this->validate($request, [
                'name' => 'required|string',
                'email' => 'required|email',
                'role' => 'required'
            ], ['required' => 'The :attribute field is required.']);

        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
        try {
            $this->usersService->updateUser(Auth::user()->user_id,$request->all());
        } catch (\Exception $e) {
            flash("User service problem");
            return back()->with('error', $e->getMessage())->withInput();
        }
        return back();

    }


    public function changePassword(Request $request)
    {

        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);



        //Change Password
        try {
            $this->usersService->changePassword($request->all());
        } catch (\Exception $e) {
            flash("User service problem");
            return back()->with('error', $e->getMessage())->withInput();
        }

        return redirect()->back()->with("success", "Password changed successfully !");

    }


    /**
     * Update User data by id
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateUser(Request $request, $id)
    {
        try {
            $this->validate($request, [
                'name' => 'required|string',
                'role' => 'required|string',
                'email' => 'required|email',
            ]);
        }catch (\Exception $e){
            return back()->with('error', $e->getMessage())->withInput();
        }
        //Change Password
        try {
            $this->usersService->updateUser($id,$request->all());
        } catch (\Exception $e) {
            flash("User service problem");
            return back()->with('error', $e->getMessage())->withInput();
        }

    }

    /**
     * Delete user by id
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteUser($id)
    {
        try {
            $this->usersService->deleteUser($id);

        } catch (\Exception $e) {

            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function logout()
    {
        //logout user
        auth()->logout();
        // redirect to homepage
        return redirect('/');
    }

}