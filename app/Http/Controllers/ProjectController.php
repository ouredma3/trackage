<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 03/05/2020
 * Time: 15:37
 */

namespace App\Http\Controllers;

use App\Services\Project;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;

class ProjectController
{
    use ApiResponser;

    public $projectService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Project $projectService)
    {
        $this->projectService = $projectService;
    }

    public function allProjects()
    {
        $projects = $this->projectService->getProjects();
        return view('projects.index')->with(['projects' => $projects]);
    }

    public function addProjects(Request $request)
    {

        $data = $request->all();
        $this->projectService->addProject($data);
        return back();
    }


    public function getProject($id)
    {
        $project = $this->projectService->getProject($id);
        $tasks = $this->projectService->getProjectTasks($id);
        foreach ($tasks as &$task) {
            if ($task->estimate != 0 && $task->actual != 0) {
                $task->percentage = round($task->actual / $task->estimate * 100 );
            } else {
                $task->percentage = 0;
            }
            $task->actual = round($task->actual / 3600);
            $task->estimate = round($task->estimate / 3600);
        }
        return view('projects.single')->with(['project' => $project, 'tasks' => $tasks]);
    }

    public function editProject(Request $request, $id)
    {
        $data = $request->all();

        try {
            $this->projectService->editProject($data, $id);
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }

        return back();
    }

    /**
     * Delete project by id
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteProject($id)
    {
        try {
            $this->projectService->deleteProject($id);

            return back();

        } catch (\Exception $e) {

            return back()->with('error', $e->getMessage())->withInput();
        }


    }

    public function addTask(Request $request)
    {
        try {
            $data = $request->all();
            if ($data['actual'] == null) {
                $data['actual'] = 0;
            } else {
                $data['actual'] = 60 * 60 * $data['actual'];
            }
            if ($data['estimate'] == null) {
                $data['estimate'] = 0;
            } else {
                $data['estimate'] = 60 * 60 * $data['estimate'];
            }

            $this->successResponse($this->projectService->addTask($data));

            return back();
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function getTask($id)
    {
        try {
            $task = $this->projectService->getTask($id);
            $task->actual = $task->actual / 3600;
            $task->estimate = $task->estimate / 3600;
            $project = $this->projectService->getProject($task->project_id);
            return view('tasks.single')->with(['project' => $project, 'task' => $task]);
        } catch (\Exception $e) {

            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function getTasks()
    {
        try {
            $tasks = $this->projectService->getTasks();
            return view('tasks.single')->with(['tasks' => $tasks]);
        } catch (\Exception $e) {

            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function editTask(Request $request, $id)
    {
        $data = $request->all();
        $data['actual'] = 60 * 60 * $data['actual'];
        $data['estimate'] = 60 * 60 * $data['estimate'];
        try {
            $this->projectService->editTask($data, $id);

        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }

        return back();
    }

    public function deleteTask($id)
    {
        try {
            $this->projectService->deleteTask( $id);

        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }

        return back();
    }
}