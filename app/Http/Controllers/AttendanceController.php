<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 26/11/2019
 * Time: 18:53
 */

namespace App\Http\Controllers;

use App\Services\Attendance;
use App\Services\Project;
use  App\User;
use App\Traits\ApiResponser;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class AttendanceController
{
    use ApiResponser;

    public $attendanceService;
    public $projectService;

    public function __construct(Attendance $attendanceService, Project $projectService)
    {
        $this->attendanceService = $attendanceService;
        $this->projectService = $projectService;
    }

    public function getMyAttendence()
    {
        $id = Auth::user()->user_id;

        try {
            $attendance = $this->attendanceService->getAttendance($id);
            $activeAttendance = $this->attendanceService->getActiveAttendance($id);
            $tasks = $this->projectService->getTasks();
            $newtasks= array();
            foreach($tasks as $task)
            {
                $newtasks[$task->project_id][] = $task;
            }
            $newprojects = array();
            $projects = $this->projectService->getProjects();
            foreach ($projects as $project){
                $newprojects[$project->id] = $project;
            }
        } catch (\Exception $e) {
            abort(401);
        }


        return view('attendance.index')->with(['attendance' => $attendance, 'activeAttendance' => $activeAttendance,'newtasks' => $newtasks,'projects' => $newprojects]);
    }

    public function getAttendance($id)
    {
        try {
            User::findOrFail($id);

        } catch (\Exception $e) {

            return back()->with('error', $e->getMessage())->withInput();
        }

        $attendance = $this->attendanceService->getAttendance($id);
        $activeAttendance = $this->attendanceService->getActiveAttendance($id);


        return view('attendance.index')->with(['attendance' => $attendance, 'activeAttendance' => $activeAttendance]);
    }

    public function startAttendance(Request $request)
    {
        $user = Auth::user();
        $id = $user->user_id;
        $user->touch();
        $data = $request->all();
        $data['from'] = date("H:i:s");
        $data['from_date'] = date("Y-m-d");

        try {
            $this->successResponse($this->attendanceService->startAttendance($data, $id));
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
        return back();
    }

    public function endAttendance(Request $request)
    {
        $user = Auth::user();
        $user->touch();
        $id = $user->user_id;
        $data = $request->all();
        $data['to'] = date("H:i:s");
        $data['to_date'] = date("Y-m-d");
        $data['from_date'] = date("Y-m-d", strtotime($data['from_date']));
        $time['amount'] = Carbon::createFromTimestamp(strtotime($data['from_date'] . ' ' . $data['from']))->diffInSeconds(Carbon::createFromTimestamp(strtotime($data['to_date']. ' ' . $data['to'])));

        try {
            $this->successResponse($this->attendanceService->endAttendance($data, $id));
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
        try {
             $this->projectService->addTime($time,$data['task_id']);
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
        return back();
    }

    public function editAttendance(Request $request, $id)
    {
        $data = $request->all();
        $data['to_date'] = date("Y-m-d", strtotime($data['to_date']));
        $data['to'] = date("H:i:s", strtotime($data['to']));
        $data['from_date'] = date("Y-m-d", strtotime($data['from_date']));
        $data['from'] = date("H:i:s", strtotime($data['from']));
        $data['project_id'] = 1;
        try {
            $this->successResponse($this->attendanceService->editAttendance($data, $id));
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }

        return redirect('attendance');
    }

    public function removeAttendance($user){
        $this->successResponse($this->attendanceService->removeAttendance($user));
        return back();
    }



}

