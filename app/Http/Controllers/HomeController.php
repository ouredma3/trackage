<?php

namespace App\Http\Controllers;

use App\Services\Project;
use Illuminate\Support\Facades\Auth;
use App\Services\Attendance;
use Illuminate\Http\Request;
use Mockery\Exception;

class HomeController extends Controller
{

    public $attendanceService;
    public $projectService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Attendance $attendanceService, Project $projectService)
    {
        $this->middleware('auth');
        $this->attendanceService = $attendanceService;
        $this->projectService = $projectService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        try{
            $tasks = $this->projectService->getTasks();
        }catch (Exception $e){
            flash('Project service offline');
            return redirect('/login');
        }

        $newtasks = array();
        foreach ($tasks as $task) {
            $newtasks[$task->id] = $task;
        }
        $tasks = $newtasks;
        try{
            $attendance = $this->attendanceService->getAttendance(Auth::user()->user_id);
            $activeAttendance = $this->attendanceService->getActiveAttendance(Auth::user()->user_id);
        }catch (Exception $e){
            flash('Attendance service offline');
            return redirect('/login');
        }

        return view('home')->with(['attendance' => $attendance, 'activeAttendance' => $activeAttendance, 'tasks' => $tasks]);
    }
}
