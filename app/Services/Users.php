<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 26/11/2019
 * Time: 19:14
 */

namespace App\Services;

use Illuminate\Support\Facades\Auth;
use App\Traits\ConsumeExternalService;
use Laravel\Lumen\Http\Request;

class Users
{
    use ConsumeExternalService;

    /**
     * The base uri to consume authors service
     * @var string
     */
    public $baseUri;


    public function __construct()
    {
        $this->baseUri = config('services.laravelpassport.host');
    }

    public function login($request)
    {
        return $this->performRequest('POST', "/oauth/token", $request);
    }

    public function allUsers()
    {
        return $this->performRequest('GET', "/api/users/", array(), [
        'Accept' => 'application/json',
        'Authorization' => 'Bearer ' . Auth::user()->access_token]);
    }

    /**
     * Get the authenticated User.
     * @var string
     *
     * @return string
     */
    public function getProfile()
    {
        return $this->performRequest('GET', "/api/users/profile", array(), [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . Auth::user()->access_token]);
    }

    public function getUser($id)
    {
        return $this->performRequest('GET', "/api/users/{$id}", array(), [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . Auth::user()->access_token]);
    }

    public function updateUser($id, $request)
    {
        return $this->performRequest('PUT', "/api/users/{$id}", $request, [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . Auth::user()->access_token]);

    }

    public function changePassword($request)
    {
        return $this->performRequest('PUT', "/api/users/password", $request, [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . Auth::user()->access_token]);

    }

    public function deleteUser($id)
    {
        return $this->performRequest('DELETE', "/api/users/{$id}", array(), [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . Auth::user()->access_token]);
    }

    public function refreshToken()
    {
        return $this->performRequest('POST',
            "/oauth/token",
            ['grant_type' => 'refresh_token',
            'refresh_token' => Auth::user()->refresh_token,
            'client_id' => config('services.laravelpassport.client_id'),
            'client_secret' => config('services.laravelpassport.client_secret'),
            'scope' => '',]);
    }


}