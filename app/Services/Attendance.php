<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 26/11/2019
 * Time: 19:14
 */

namespace App\Services;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Traits\ConsumeExternalService;

class Attendance
{
    use ConsumeExternalService;

    /**
     * The base uri to consume authors service
     * @var string
     */
    public $baseUri;

    /**
     * Authorization secret to pass to author api
     * @var string
     */
    public $secret;

    public function __construct()
    {
        $this->baseUri = config('services.attendance.base_uri');
        $this->secret = config('services.attendance.secret');
    }




    /**
     * Obtain user attendance
     */
    public function getAttendance($id)
    {
        $attendance = $this->performRequest('GET', "/attendances/{$id}");
        $attendance = json_decode($attendance);

        $sorted_attendance = array();
        foreach ($attendance as &$item){

                $from = strtotime($item->from);
                $from_date = strtotime($item->from_date);
                $day = date('D', $from);
                $date_key = $from_date;
                $date = date('d F', $from_date);
                if (!isset($sorted_attendance[$date_key])) {
                    $sorted_attendance[$date_key]['items'] = array();
                    $sorted_attendance[$date_key]['date'] = $date;
                    $sorted_attendance[$date_key]['day'] = $day;
                    $sorted_attendance[$date_key]['total'] = 0;
                }
                $sorted_attendance[$date_key]['total'] += $item->time;
                $item = (array)$item;
                array_push($sorted_attendance[$date_key]['items'], $item);

        }
        krsort($sorted_attendance);
        return $sorted_attendance;
    }

    public function getActiveAttendance($id){
        return (array)json_decode($this->performRequest('GET', "/attendances/{$id}/active"));
    }

    public function startAttendance($data,$id )
    {
        return $this->performRequest('POST', "/attendances/{$id}/start", $data);
    }

    public function endAttendance($data , $id)
    {
        return $this->performRequest('POST', "/attendances/{$id}/end", $data);
    }

    public function editAttendance($data, $id)
    {
        return $this->performRequest('PUT', "/attendances/{$id}", $data);
    }

    public function removeAttendance($user){
        return $this->performRequest('DELETE', "/attendances/{$user}");
    }

}