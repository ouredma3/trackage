<?php

namespace App\Traits;

use GuzzleHttp\Client;

trait ConsumeExternalService
{
    /**
     * Send request to any service
     * @param $method
     * @param $requestUrl
     * @param array $formParams
     * @param array $headers
     * @return string
     */
    public function performRequest($method, $requestUrl, $formParams = [], $headers = [])
    {

        $client = new Client([
            'base_uri' => $this->baseUri,
        ]);

        if (isset($this->secret)) {
            $headers['Authorization'] = $this->secret;
        }
        try {
            $response = $client->request($method, $requestUrl, [
                'form_params' => $formParams,
                'headers' => $headers,
            ]);
        } catch (\Exception $e) {
            abort($e->getCode());
        }

        return $response->getBody()->getContents();
    }
}